# A short description of the tile

[![Latest Version on Packagist](https://img.shields.io/packagist/v/jonlynch/response-tile.svg?style=flat-square)](https://packagist.org/packages/jonlynch/rresponse-tile)

This tile can be used on [the Laravel Dashboard](https://docs.spatie.be/laravel-dashboard).

## Installation

You can install the package via composer:

```bash
$ composer require jonlynch/response-tile
```

## Usage

In your dashboard view you use the `livewire:response-tile` component.

```html
<x-dashboard>
    <livewire:response-tile position="a1:a2"/>
</x-dashboard>
```

Add the config to the tiles sections of your `config/dashboard.php`

```php
// in config/dashboard.php

return [
    // ...
    tiles => [
        'response' => [
            'url' => env('RESPONSE_URL'),
        ],
    ]
```

In app\Console\Kernel.php you should schedule the JonLynch\UkWeatherTile\Commands\FetchMetOfficeDataCommand to run every minute.

``` php
// in app\Console\Kernel.php

protected function schedule(Schedule $schedule)
{
    $schedule->command(\JonLynch\UkWeatherTile\Commands\FetchResponseDataCommand::class)->everyMinute();
}
```
## Security

If you discover any security related issues, please email :author_email instead of using the issue tracker.

## Credits

- [Jon Lynch](https://github.com/jonlynch)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
