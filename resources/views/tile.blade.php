<x-dashboard-tile :position="$position">
    <div class="h-full justify-items-center">
        <h1 class="font-medium text-dimmed text-sm text-center uppercase tracking-wide pb-2">
            Responses in last 24hr
        </h1>
        <div wire:poll.{{ $refreshIntervalInSeconds }}s class="self-center flex flex-wrap flex max-h-full">
            
            @foreach ($responses as $response)

                <div class="w-1/{{$cols}} max ">
                    <div class="m-1 p-1 bg-gray-100 rounded block border-l-8 shadow
                    @if ($response['response_type'] === 'Available') border-green-500 
                    @elseif ($response['response_type'] === 'Limited') border-yellow-500 
                    @else border-red-500 @endif
                   ">
                        <div class="block ml-1 whitespace-no-wrap overflow-hidden">{{$response['name']}}</div>
                        
                        @if (isset($response['etaForHumans']))
                            <div class="block ml-1 text-dimmed text-xs ">eta {{$response['etaForHumans']}}</div>
                        @endif 
                        <!--<div class="block ml-1 text-dimmed text-xs ">{{$response['message']}}</div>-->
                    
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</x-dashboard-tile>
