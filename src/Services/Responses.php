<?php

namespace JonLynch\ResponseTile\Services;

use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;
use JonLynch\ResponseTile\Response;
use Carbon\Carbon;

class Responses
{
    public static function getResponses(string $url): array
    {
        $body = Http::get($url)->body();
        // fix required to handle invalid HTML on response page
        $body = str_replace("</body>", "</div></body>", $body);
        $crawler = new Crawler($body);

        if (count($crawler->filter('table')) < 3) 
        {
            return [];
        }

        $responseTable = $crawler->filter('table')->last();

        // remove header row
        $responseTable->filter('tr')->each(
            function (Crawler $tr, $i) {
                foreach ($tr as $node) {
                    if ($i == 0) {
                        $node = $tr->getNode(0);
                        $node->parentNode->removeChild($node);
                    }
                }
            }
        );
        
        $responses = $responseTable->filter('tr')->each(
            function ($tr, $i) {
                $response = new Response();
                $tds = $tr->filter('td');

                // todo move all this onto the model
                $response->setResponseTypeFromColour($tds->first()->attr('bgcolor'));
                $response->message = trim(str_replace('(Web)', '', $tds->eq(4)->text('')));
                $response->name = trim($tds->eq(1)->text(''));
                $responseTime = trim($tds->eq(5)->text(''));
                if ($responseTime <> '') {
                    $dt = Carbon::now();
                    preg_match_all('/(\d{1,2})$/', $responseTime, $matches);
                    $dt->year = intval('20' . $matches[1][0]);
                    preg_match_all('/\/(\d{1,2})\//', $responseTime, $matches);
                    $dt->month = intval($matches[1][0]);
                    preg_match_all('/ (\d{1,2})\//', $responseTime, $matches);
                    $dt->day =  intval($matches[1][0]);
                    preg_match_all('/^(\d{1,2})/', $responseTime, $matches);
                    $dt->hour =  intval($matches[1][0]);
                    preg_match_all('/:(\d{1,2}):/', $responseTime, $matches);
                    $dt->minute =  intval($matches[1][0]);
                    preg_match_all('/:(\d{1,2})hrs/', $responseTime, $matches);
                    $dt->second =  intval($matches[1][0]);
                    $dt->setTimezone('Europe/London');
                    $response->responded_at = $dt;
                    $response->respondedForHumans = $dt->diffForHumans();
                }
                preg_match_all('/\d{1,3}/', trim($tds->eq(3)->text('')), $matches);
                if ((isset($matches[0])) && (isset($matches[0][0]))) {
                    $response->eta = $dt->copy()->addMinutes((int)$matches[0][0]);
                    $response->etaForHumans = $response->eta->diffForHumans();
                }
                
                return $response->toArray();
            }
        );

        $collection = collect($responses);
        
        $sorted =$collection->sortBy(function ($response) {
            if (isset($response['eta'])) {
                return $response['eta']->format('U');
            }
            if ($response['response_type'] == 'Available') {
                return $response['responded_at']->addHours(1)->format('U');
            }
            if ($response['response_type'] == 'Limited') {
                return $response['responded_at']->addHours(2)->format('U');
            }
            if ($response['response_type'] == 'Not Available') {
                return $response['responded_at']->addDays(1)->format('U');
            }
            
            return $response['responded_at']->addDays(2)->format('U');
        });
        return $sorted->values()->all();
    }
}
