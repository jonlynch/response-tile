<?php

namespace JonLynch\ResponseTile;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Response
 *
 * 
 * @property \Carbon\Carbon $responded_at
 * @property String $response_type */

class Response extends Model
{

    protected $fillable = [
        'responded_at',
        'name',
        'response_type',
        'eta',
        'message'
    ];

    public function createResponseFromHTML(Array $HTML, $response) {
        $this->message = $HTML[3];
        $this->response = $response;
    }

    public function setResponseTypeFromColour(String $colour) {
        switch ($colour) {
            case "red":
                $this->response_type = "Not Available";
                break;
            case "limegreen":
                $this->response_type = "Available";
                break;
            case "yellow":
                $this->response_type = "Limited";
                break;
            case "white":
                $this->response_type = "At Home";
                break;
            }
    }

}
