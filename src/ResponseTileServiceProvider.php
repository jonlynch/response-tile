<?php

namespace JonLynch\ResponseTile;

use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use JonLynch\ResponseTile\Commands\FetchResponseDataCommand;

class ResponseTileServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                FetchResponseDataCommand::class,
            ]);
        }

        $this->publishes([
            __DIR__ . '/../resources/views' => resource_path('views/vendor/dashboard-response-tile'),
        ], 'dashboard-response-tile-views');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'dashboard-response-tile');

        Livewire::component('response-tile', ResponseTileComponent::class);
    }
}
