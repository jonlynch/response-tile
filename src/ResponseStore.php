<?php

namespace JonLynch\ResponseTile;

use Spatie\Dashboard\Models\Tile;

class ResponseStore
{
    private Tile $tile;

    public static function make()
    {
        return new static();
    }

    public function __construct()
    {
        $this->tile = Tile::firstOrCreateForName("response");
    }

    public function setResponses(array $data): self
    {
        $this->tile->putData('response', $data);

        return $this;
    }

    public function responses(): array
    {
        return $this->tile->getData('response') ?? [];
    }
}
