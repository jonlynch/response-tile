<?php

namespace JonLynch\ResponseTile;

use Livewire\Component;

class ResponseTileComponent extends Component
{
    /** @var string */
    public $position;

    public $cols;

    public function mount(string $position, string $cols)
    {
        $this->position = $position;
        $this->cols = $cols;
    }

    public function render()
    {
        return view('dashboard-response-tile::tile', [
            'responses' => ResponseStore::make()->responses(),
            'refreshIntervalInSeconds' => config('dashboard.tiles.responses.refresh_interval_in_seconds') ?? 15
        ]);
    }
}
