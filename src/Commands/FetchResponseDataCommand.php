<?php

namespace JonLynch\ResponseTile\Commands;

use Illuminate\Console\Command;
use JonLynch\ResponseTile\ResponseStore;
use JonLynch\ResponseTile\Services\Responses;

class FetchResponseDataCommand extends Command
{
    protected $signature = 'dashboard:fetch-response-data';

    protected $description = 'Fetch response data';

    public function handle()
    {
        $this->info('Fetching response data');

        $responses = Responses::getResponses(
            config('dashboard.tiles.response.url'),
        );

        ResponseStore::make()->setResponses($responses);
        
        $this->info('All done!');
    }
}
